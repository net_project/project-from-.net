﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Emgu.CV.Structure;
using Emgu.CV;
using Project_Database.Biometrics;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace ProjectTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // Phase 1 - Construction
            Image<Bgr, Byte> compare = new Image<Bgr, Byte>("C:/Users/Ada/Documents/Visual Studio 2012/Projects/Database_Project/Database_Project/bin/Debug/fejsys/twarz2.jpg");
            Image<Bgr, Byte> random = new Image<Bgr, Byte>("C:/Users/Ada/Documents/Visual Studio 2012/Projects/Database_Project/Database_Project/bin/Debug/fejsys/twarz3.jpg");
            Image<Bgr, Byte> tofind = new Image<Bgr, Byte>("C:/Users/Ada/Documents/Visual Studio 2012/Projects/Database_Project/Database_Project/bin/Debug/fejsys/twarz5.jpg");
            // Phase 2 - Execution    
            //init fields
            Biometrics_Functions bf = new Biometrics_Functions();
            List<Image<Gray, byte>> imageList = new List<Image<Gray, byte>>();
            List<string> labels = new List<string>();
            bf.AddFaceFromPictureToList(compare, imageList);
            bf.AddFaceFromPictureToList(random, imageList);
            labels.Add("a");
            labels.Add("b");
            string result = bf.FindFace(imageList, labels, tofind);
            // Phase 3 - Assert
            Assert.AreEqual("a", result);
        }
    }
}
