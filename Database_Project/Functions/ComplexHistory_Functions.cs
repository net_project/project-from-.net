﻿using Project_Database.Contexts;
using Project_Database.DataSets;
using Project_Database.Repository;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CodeFirstDataBase;
using Project_Database.Enums;

namespace Project_Database.Functions
{
    public class ComplexHistory_Functions
    {
        //function receiving his_data, filling it and returning
        public static async Task<List<History_DataSet>> Get(Person_DataSet per_data)
        {
            List<History_DataSet> listOfHist = new List<History_DataSet>();
            History_DataSet hist = new History_DataSet();
            using (var db = new Project_DbContext())
            {
                var usershistory = db.Histories.Where(p => p.User.PK_ID == per_data.uid);
                foreach (History item in usershistory)
                {
                    hist.date = item.Date;
                    hist.eyecolor = item.People.EyeColor.Color;
                    hist.haircolor = item.People.HairColor.Color;
                    hist.image = item.People.ProfileImage;
                    hist.name = item.People.Name;
                    hist.surname = item.People.Surname;
                    hist.username = item.User.Username; //returns the searcher!
                    // hist.username = item.People.User.Username; //returns the searched!
                    hist.dstype = DataSetType_Enum.HistoryDS;
                    listOfHist.Add(hist);
                }
            }
            return listOfHist;
        }
        //function receiving his_data and removing it based on the removeFalse boolean
        public static async Task<List<History_DataSet>> Delete(Person_DataSet per_data)
        {
            List<History_DataSet> listOfHist = new List<History_DataSet>();
            using (var db = new Project_DbContext())
            {
                var usershistorytodelete = db.Histories.Where(p => p.User.PK_ID == per_data.uid);
                usershistorytodelete = usershistorytodelete.Where(p => p.People.isReal == false);
                var HistoryRepository = new History_Repository(db);
                foreach (var item in usershistorytodelete)
                {
                    HistoryRepository.Delete(item);
                }
                await db.SaveChangesAsync();
            }
            return await Get(per_data);
        }
        //function receiving his_data and removing it all
        public static async Task<List<History_DataSet>> DeleteAll(Person_DataSet per_data)
        {
            using (var db = new Project_DbContext())
            {
                var HistoryRepository = new History_Repository(db);
                var PeopleRepository = new People_Repository(db);
                var history = db.Histories.Where(h => h.User.PK_ID == per_data.uid);
                foreach (var hist in history)
                {
                    if (hist.People.isReal == false)
                    {
                        //UserRepository.Delete(hist.People.User); to check!
                        PeopleRepository.Delete(hist.People);
                    }
                    HistoryRepository.Delete(hist); //if history exist-delete
                }
                await db.SaveChangesAsync();
            }
            return new List<History_DataSet>();
        }
    }
}
