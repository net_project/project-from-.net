﻿using CodeFirstDataBase;
using Project_Database.DataSets;
using Project_Database.Contexts;
using Project_Database.Repository;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Project_Database.Converters;
using Emgu.CV;
using Emgu.CV.Structure;
using Project_Database.Biometrics;
using Project_Database.Enums;

namespace Project_Database.Functions
{
    public class ComplexPerson_Functions
    {
        public static async Task<Person_DataSet> HelpRegister(Person_DataSet per_data)
        {
            using (var db = new Project_DbContext())
            {
                var PeopleRepository = new People_Repository(db);
                var person = new People { Name = per_data.name, Surname = per_data.surname, isReal = per_data.isReal };
                if (per_data.eyecolor != null)
                {
                    var eyecolor = new EyeColor { Color = per_data.eyecolor };
                    var eyecolordb = await db.EyeColors.Where(x => x.Color == eyecolor.Color).FirstOrDefaultAsync();
                    if (eyecolordb == null || object.Equals(eyecolordb, default(EyeColor)))    // doesn't exist - firstOrDefault() returns default
                    {
                        db.EyeColors.Add(eyecolor);
                    }
                    else
                    {
                        db.EyeColors.Attach(eyecolordb);
                        eyecolor = eyecolordb;
                    }
                    person.EyeColor = eyecolor;
                }
                else
                {
                    person.EyeColor = null;
                }
                if (per_data.haircolor != null)
                {
                    var haircolor = new HairColor { Color = per_data.haircolor };
                    var haircolordb = await db.HairColors.Where(x => x.Color.Contains(haircolor.Color)).FirstOrDefaultAsync();
                    if (haircolordb == null || object.Equals(haircolordb, default(HairColor)))
                    {
                        db.HairColors.Add(haircolor);
                    }
                    else
                    {
                        db.HairColors.Attach(haircolordb);
                        haircolor = haircolordb;
                    }
                    person.HairColor = haircolor;
                }
                else
                {
                    person.HairColor = null;
                }
                var image = new Image { image = per_data.image };
                db.Images.Add(image);
                var user = new User { Username = per_data.username, Password = per_data.password };
                db.Users.Add(user);
                image.People = person;
                person.Images.Add(image);
                person.ProfileImage = image.image;
                person.User = user;
                PeopleRepository.AddToContext(person);
                await db.SaveChangesAsync();
                per_data.id = person.PK_ID;
                per_data.uid = person.User.PK_ID;
            }
            per_data.dstype = DataSetType_Enum.PersonDS;
            return per_data;
        }
        //function receiving per_data, filling db and returning the data
        public static async Task<Person_DataSet> Register(Person_DataSet per_data)
        {
            using (var db = new Project_DbContext())
            {
                var PeopleRepository = new People_Repository(db);
                var usernamedb = await db.Users.Where(u => u.Username == per_data.username).FirstOrDefaultAsync();
                if (usernamedb == null || object.Equals(usernamedb, default(User)))    // doesn't exist - firstOrDefault() returns default
                {
                    per_data = await HelpRegister(per_data);
                }
                else
                {
                    per_data.username=null; //if username taken, return null
                }
            }
            per_data.dstype = DataSetType_Enum.PersonDS;
            return per_data;
        }
        //there is a user- want's to update the profile admin created
        public static async Task<Person_DataSet> Update(Person_DataSet per_data)
        {
            using (var db = new Project_DbContext())
            {
                var PeopleRepository = new People_Repository(db);
                var person = new People { Name = per_data.name, Surname = per_data.surname, isReal = per_data.isReal };
                var usernamedb = await db.Users.Where(u => u.PK_ID != per_data.uid && u.Username == per_data.username).FirstOrDefaultAsync();
                if (usernamedb == null || object.Equals(usernamedb, default(User)))    // doesn't exist - firstOrDefault() returns default
                {
                    var user = await db.Users.Where(u => u.PK_ID == per_data.uid).FirstOrDefaultAsync();
                    person.User = user;
                }
                else
                {
                    per_data.username = null; //if username taken, return null
                }
                if (per_data.eyecolor != null)
                {
                    var eyecolor = new EyeColor { Color = per_data.eyecolor };
                    var eyecolordb = await db.EyeColors.Where(x => x.Color == eyecolor.Color).FirstOrDefaultAsync();
                    if (eyecolordb == null || object.Equals(eyecolordb, default(EyeColor)))    // doesn't exist - firstOrDefault() returns default
                    {
                        db.EyeColors.Add(eyecolor);
                    }
                    else
                    {
                        db.EyeColors.Attach(eyecolordb);
                        eyecolor = eyecolordb;
                    }
                    person.EyeColor = eyecolor;
                }
                else
                {
                    person.EyeColor = null;
                }
                if (per_data.haircolor != null)
                {
                    var haircolor = new HairColor { Color = per_data.haircolor };
                    var haircolordb = await db.HairColors.Where(x => x.Color.Contains(haircolor.Color)).FirstOrDefaultAsync();
                    if (haircolordb == null || object.Equals(haircolordb, default(HairColor)))
                    {
                        db.HairColors.Add(haircolor);
                    }
                    else
                    {
                        db.HairColors.Attach(haircolordb);
                        haircolor = haircolordb;
                    }
                    person.HairColor = haircolor;
                }
                else
                {
                    person.HairColor = null;
                }
                var image = new Image { image = per_data.image };
                db.Images.Add(image);
                image.People = person;
                person.Images.Add(image);
                person.ProfileImage = image.image;
                PeopleRepository.AddToContext(person);
                await db.SaveChangesAsync();
                per_data.id = person.PK_ID;
                per_data.uid = person.User.PK_ID;
            }
            per_data.dstype = DataSetType_Enum.PersonDS;
            return per_data;
        }
        //function receiving per_data, filling it and returning
        public static async Task<Person_DataSet> LogIn(Person_DataSet per_data)
        {
            using(var db = new Project_DbContext())
            {
                var user = await db.Users.Where(x => x.Username == per_data.username && x.Password == per_data.password).FirstOrDefaultAsync();
                if (user == null || object.Equals(user, default(User)))    // doesn't exist - firstOrDefault() returns default
                {
                    return null;
                }
                var person = await db.Peoples.Where(p => p.User.PK_ID == user.PK_ID).FirstOrDefaultAsync();
                if (person == null || object.Equals(person, default(People)))    // doesn't exist - firstOrDefault() returns default
                {
                    return per_data; //no person data found - ask to fill in the profile(?)
                }
                per_data.id = person.PK_ID;
                per_data.uid = person.User.PK_ID;
                per_data.name = person.Name;
                per_data.surname = person.Surname;
                per_data.isReal = person.isReal;
                per_data.eyecolor = person.EyeColor.Color;
                per_data.haircolor = person.HairColor.Color;
                if (person.Images.Count != 0)
                {
                    per_data.image = person.ProfileImage;
                }
                else
                {
                    per_data.image = null;
                }
            }
            per_data.dstype = DataSetType_Enum.PersonDS;
            return per_data;
        }
        //function receiving per_data, filling db and returning the data
        public static async Task<Person_DataSet> AdminAdd(Person_DataSet admin, Person_DataSet per_data)
        {
            using(var db = new Project_DbContext())
            {
                var admin_data = await db.Peoples.Where(p => p.PK_ID == admin.id && p.User.isAdmin == true).FirstOrDefaultAsync();
                if (admin_data == null || object.Equals(admin_data, default(People)))
                {
                    return null; //if not admin requests return null
                }
                //var usernamedb = await db.Users.Where(u => u.Username == per_data.username).FirstOrDefaultAsync();
                //if (usernamedb == null || object.Equals(usernamedb, default(People)))
                //{
                //}
                //else
                //{
                //    return null;
                //}
                per_data = await HelpRegister(per_data);
            }
            per_data.dstype = DataSetType_Enum.PersonDS;
            return per_data;
        }

        public static async Task<Person_DataSet> HistoryAdd( Person_DataSet per_data)
        {
            using (var db = new Project_DbContext())
            {
                per_data = await HelpRegister(per_data);
            }
            per_data.dstype = DataSetType_Enum.PersonDS;
            return per_data;
        }

        public static People DataSet_People(Person_DataSet per_data, People person)
        {
            person.Name = per_data.name;
            person.Surname = per_data.surname;
            person.isReal = per_data.isReal;
            person.EyeColor.Color = per_data.eyecolor;
            person.HairColor.Color = per_data.haircolor;
            person.ProfileImage = per_data.image;
            person.User.Username = per_data.username;
            person.User.Password = per_data.password;
            return person;
        }

        public static Person_DataSet People_DataSet(Person_DataSet per_data, People person)
        {
            per_data.name = person.Name;
            per_data.surname = person.Surname;
            per_data.isReal = person.isReal;
            per_data.eyecolor = person.EyeColor.Color;
            per_data.haircolor = person.HairColor.Color;
            per_data.image = person.ProfileImage;
            per_data.username = person.User.Username;
            per_data.id = person.PK_ID;
            per_data.uid = person.User.PK_ID;
            per_data.dstype = DataSetType_Enum.PersonDS;
            return per_data;
        }

        //function receiving per_data, reading it, changing db and returning
        public static async Task<Person_DataSet> Edit(Person_DataSet per_data) //????????
        {
            using (var db = new Project_DbContext())
            {
                var PeopleRepository = new People_Repository(db);
                var person = await db.Peoples.Where(x => x.PK_ID == per_data.id).FirstOrDefaultAsync();
                var usernamedb = await db.Users.Where(u => u.PK_ID != per_data.uid && u.Username == per_data.username).FirstOrDefaultAsync();
                if (usernamedb == null || object.Equals(usernamedb, default(User)))    // doesn't exist - firstOrDefault() returns default
                {
                    //person.User.Username = per_data.username;
                    //person.User.Password = per_data.password;
                }
                else
                {
                    return null; //return null if username taken
                }
                if (person == null || object.Equals(person, default(User)))    // doesn't exist - firstOrDefault() returns default
                {
                    return await Update(per_data);
                }
                if (per_data.eyecolor != null)
                {
                    var eyecolor = new EyeColor { Color = per_data.eyecolor };
                    var eyecolordb = await db.EyeColors.Where(x => x.Color == eyecolor.Color).FirstOrDefaultAsync();
                    if (eyecolordb == null || object.Equals(eyecolordb, default(EyeColor)))    // doesn't exist - firstOrDefault() returns default
                    {
                        db.EyeColors.Add(eyecolor);
                    }
                    else
                    {
                        db.EyeColors.Attach(eyecolordb);
                        eyecolor = eyecolordb;
                    }
                    person.EyeColor = eyecolor;
                }
                else
                {
                    person.EyeColor = null;
                }
                if (per_data.haircolor != null)
                {
                    var haircolor = new HairColor { Color = per_data.haircolor };
                    var haircolordb = await db.HairColors.Where(x => x.Color.Contains(haircolor.Color)).FirstOrDefaultAsync();
                    if (haircolordb == null || object.Equals(haircolordb, default(HairColor)))
                    {
                        db.HairColors.Add(haircolor);
                    }
                    else
                    {
                        db.HairColors.Attach(haircolordb);
                        haircolor = haircolordb;
                    }
                    person.HairColor = haircolor;
                }
                else
                {
                    person.HairColor = null;
                }
                if (per_data.image != null)
                {
                    person.ProfileImage = per_data.image;
                }
                PeopleRepository.Update(DataSet_People(per_data, person)); // we change profile picture only
                await db.SaveChangesAsync();
            }
            per_data.dstype = DataSetType_Enum.PersonDS;
            return per_data;
        }
        //function receiving per_data, deleting it and returning empty class
        public static async Task<Person_DataSet> Delete(Person_DataSet per_data)
        {
            using (var db = new Project_DbContext())
            {
                var PeopleRepository = new People_Repository(db);
                var ImageRepository = new Image_Repository(db);
                var HistoryRepository = new History_Repository(db);
                var UserRepository = new User_Repository(db);
                var person = await db.Peoples.Where(p => p.PK_ID == per_data.id).FirstOrDefaultAsync();
                if (person == null || object.Equals(person, default(People)))    // doesn't exist - firstOrDefault() returns default
                {
                    return null; //if person doesn't exist- don't delete
                }
                var images = await db.Images.Where(i => i.People.PK_ID == person.PK_ID).ToListAsync();
                foreach (var img in images)
                {
                    ImageRepository.Delete(img); //if pictures exist-delete
                }
                var history = await db.Histories.Where(h => h.User.PK_ID == per_data.uid).ToListAsync();
                foreach(var hist in history)
                {
                    if (hist.People.isReal == false)
                    {
                        //UserRepository.Delete(hist.People.User); to check!
                        PeopleRepository.Delete(hist.People);
                    }
                    HistoryRepository.Delete(hist); //if history exist-delete
                }
                var user = await db.Users.Where(u => u.PK_ID == per_data.uid).FirstOrDefaultAsync();
                if (user == null || object.Equals(user, default(User)))    // doesn't exist - firstOrDefault() returns default
                {
                }
                else
                {
                    UserRepository.Delete(user); //if history exist-delete
                }
                PeopleRepository.Delete(person);
                await db.SaveChangesAsync();
            }
            per_data.dstype = DataSetType_Enum.PersonDS;
            return per_data;
        }
        //function receiving per_data, filling it and returning, think how to ASYNC!!!!!
        public static async Task<Person_DataSet> Search(Person_DataSet searcher, Person_DataSet per_data) //add compare image
        {
            using (var db = new Project_DbContext())
            {
                ByteImage_Converters conv = new ByteImage_Converters();
                var pictures = new List<Tuple<string, Image<Bgr, Byte>>>();
                var query = db.Peoples.AsEnumerable();
                if (db.Peoples.Count() == 0)
                    return null;
                query = db.Peoples.Where(x => x.isReal == true);
                if(per_data.name!=null)
                    query = query.Where(x => x.Name == per_data.name);
                if(per_data.surname!=null)
                    query = query.Where(x => x.Surname == per_data.surname);
                if (per_data.username != null)
                    query = query.Where(x => x.User.Username == per_data.username);
                if (per_data.haircolor != null)
                    query = query.Where(x => x.HairColor.Color == per_data.haircolor);
                if (per_data.eyecolor != null)
                    query = query.Where(x => x.EyeColor.Color == per_data.eyecolor);
                if (per_data.image != null)
                {
                    foreach (var item in query)
                    {
                        pictures.Add(new Tuple<string, Image<Bgr, Byte>>(item.PK_ID.ToString(), conv.ByteToBgr(item.ProfileImage))); //we search by profile image, so as not to have unnecessary replications
                    }
                    Biometrics_Functions bf = new Biometrics_Functions();
                    int personId = bf.FindPerson(pictures, per_data);
                    if (personId == 0)
                    {
                        var PeopleRepository = new People_Repository(db);
                        var HistoryRepository = new History_Repository(db);
                        var UserRepository = new User_Repository(db);
                        per_data.isReal = false;
                        var notfound = await HistoryAdd(per_data);
                        var notfoundPer = PeopleRepository.GetById(notfound.id);
                        var searchPer = UserRepository.GetById(searcher.uid);
                        History hist = new History();
                        hist.Date = DateTime.Now;
                        hist.People = notfoundPer;
                        hist.User = searchPer;
                        searchPer.Histories.Add(hist);
                        HistoryRepository.AddToContext(hist);
                        await db.SaveChangesAsync();
                        return null;
                    }
                    else
                    {
                        var PeopleRepository = new People_Repository(db);
                        var HistoryRepository = new History_Repository(db);
                        var UserRepository = new User_Repository(db);
                        var searchPer = UserRepository.GetById(searcher.uid);
                        var foundPer = PeopleRepository.GetById(personId);
                        var resultDS = new Person_DataSet();
                        History hist = new History();
                        hist.Date = DateTime.Now;
                        hist.People = foundPer;
                        hist.User = searchPer;
                        searchPer.Histories.Add(hist);
                        HistoryRepository.AddToContext(hist);
                        await db.SaveChangesAsync();
                        resultDS = People_DataSet(resultDS, foundPer);
                        return resultDS;
                    }
                }
            }
            per_data.dstype = DataSetType_Enum.PersonDS;
            return per_data;
        }
    }
}
