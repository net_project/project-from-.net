﻿using CodeFirstDataBase;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Contexts
{
    public class Project_DbContext : DbContext
    {
        //protected override bool ShouldValidateEntity(DbEntityEntry entityEntry)
        //{
        //    if ((entityEntry.Entity is Image))
        //        return false;
        //    return base.ShouldValidateEntity(entityEntry);
        //} 
        static Project_DbContext()
        {
#if DEBUG
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<Project_DbContext>());
#endif
        }
        protected override bool ShouldValidateEntity(DbEntityEntry entityEntry)
        {
            //Replace "DataClass" with the class that needs to store large data types
            if (entityEntry.Entity is People || entityEntry.Entity is Image)
            {
                return false;
            }
            return base.ShouldValidateEntity(entityEntry);
        }
        public DbSet<EyeColor> EyeColors { get; set; }
        public DbSet<HairColor> HairColors { get; set; }
        public DbSet<History> Histories { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<People> Peoples { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<People>()
                .HasOptional(a => a.User)
                .WithOptionalDependent()
                .WillCascadeOnDelete(true);
        }
    }
}
