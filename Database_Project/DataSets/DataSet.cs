﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project_Database.Enums;

namespace Project_Database.DataSets
{
    public class DataSet
    {
        public DataSetType_Enum dstype;
        public OperationTypeHistory_Enum hop;
        public OperationTypePerson_Enum pop;
    }
}
