﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project_Database.Enums;

namespace Project_Database.DataSets
{
    public class History_DataSet : DataSet
    {
        //to specify user that wants to see his history
        public string username;
        bool removeRealFlag;
        //history history data
        public DateTime date;
        public string name;
        public string surname;
        public string eyecolor;
        public string haircolor;
        public byte[] image;
    }
}
