﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project_Database.Enums;

namespace Project_Database.DataSets
{
    public class Person_DataSet : DataSet
    {
        public int id;
        public int uid;
        public string name;
        public string surname;
        public bool isReal;
        public string eyecolor;
        public string haircolor;
        public string username;
        public string password;
        public byte[] image;
    }
}
