﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Project_Database.DataSets;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project_Database.Converters;

namespace Project_Database.Biometrics
{
    public class Biometrics_Functions
    {
        public int FindPerson(List<Tuple<string, Image<Bgr, Byte>>> List, Person_DataSet per_data)
        {
            ByteImage_Converters conv = new ByteImage_Converters();
            List<Image<Gray, byte>> imageList = new List<Image<Gray,byte>>();
            List<string> indexList = new List<string>();
            foreach (var image in List)
            {
                AddFaceFromPictureToList(image.Item2, imageList);
                indexList.Add(image.Item1);
            }
            Image<Bgr, Byte> checkImage = conv.ByteToBgr(per_data.image);
            var index = FindFace(imageList, indexList, checkImage);
            int resultIndex;
            bool success = Int32.TryParse(index, out resultIndex);
            if (success)
            {
                return resultIndex;
            }
            else
            {
                return 0;// handle the case that the string doesn't contain a valid number
            }
        }

        public void AddFaceFromPictureToList(Image<Bgr, Byte> img, List<Image<Gray, byte>> ImageList)
        {
            HaarCascade face = new HaarCascade("haarcascade_frontalface_default.xml");
            Image<Gray, byte> result = null;
            MCvAvgComp[][] facesDetected = img.DetectHaarCascade(face, 1.2, 10, Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.DO_CANNY_PRUNING, new Size(20, 20));
            foreach (MCvAvgComp f in facesDetected[0])
            {
                result = img.Copy(f.rect).Convert<Gray, byte>().Resize(100, 100, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

                img.Draw(f.rect, new Bgr(Color.Red), 2);
            }
            ImageList.Add(result);
        }

        public String FindFace(List<Image<Gray, byte>> ImageList1, List<string> indexes, Image<Bgr, Byte> target)
        {
            Image<Gray, byte> result = null;
            HaarCascade face = new HaarCascade("haarcascade_frontalface_default.xml");
            string name = null;

            MCvAvgComp[][] facesDetected = target.DetectHaarCascade(face, 1.2, 10, Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.DO_CANNY_PRUNING, new Size(20, 20));
            foreach (MCvAvgComp f in facesDetected[0])
            {
                result = target.Copy(f.rect).Convert<Gray, byte>().Resize(100, 100, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

                MCvTermCriteria termCrit = new MCvTermCriteria(indexes.Count, 0.001);

                //Eigen face recognizer
                EigenObjectRecognizer recognizer = new EigenObjectRecognizer(ImageList1.ToArray(), indexes.ToArray(), 2000, ref termCrit);

                name = recognizer.Recognize(result);
            }

            return name;
        }
    }
}
