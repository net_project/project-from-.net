﻿using CodeFirstDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Interfaces
{
    public interface IHistory : IRepository<History, int>
    {
        Task<IEnumerable<History>> FindAsync(int number);
        Task<IEnumerable<History>> FindAllAsync();
    }
}
