﻿using CodeFirstDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Interfaces
{
    public interface IPeople : IRepository<People, int>
    {
        Task<IEnumerable<People>> FindAsync(int number);
        Task<IEnumerable<People>> FindAllAsync();
    }
}
