﻿using CodeFirstDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Interfaces
{
    public interface IEyeColor : IRepository<EyeColor, int>
    {
        Task<IEnumerable<EyeColor>> FindAsync(int number);
        Task<IEnumerable<EyeColor>> FindAllAsync();
    }
}
