﻿using CodeFirstDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Interfaces
{
    public interface IImage : IRepository<Image, int>
    {
        Task<IEnumerable<Image>> FindAsync(int number);
        Task<IEnumerable<Image>> FindAllAsync();
    }
}
