﻿using CodeFirstDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Interfaces
{
    public interface IUser : IRepository<User, int>
    {
        Task<IEnumerable<User>> FindAsync(int number);
        Task<IEnumerable<User>> FindAllAsync();
    }
}
