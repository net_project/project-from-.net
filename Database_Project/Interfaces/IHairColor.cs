﻿using CodeFirstDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Interfaces
{
    public interface IHairColor : IRepository<HairColor, int>
    {
        Task<IEnumerable<HairColor>> FindAsync(int number);
        Task<IEnumerable<HairColor>> FindAllAsync();
    }
}
