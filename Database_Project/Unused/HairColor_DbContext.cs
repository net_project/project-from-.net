﻿using CodeFirstDataBase;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Contexts
{
    public class HairColor_DbContext : DbContext
    {
        public DbSet<HairColor> HairColors { get; set; }
    }
}
