﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Enums
{
    public enum PictureType_Enum
    {
        JPG,
        PNG,
        GIF
    }
}
