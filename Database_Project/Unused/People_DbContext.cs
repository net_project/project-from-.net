﻿using CodeFirstDataBase;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Contexts
{
    public class People_DbContext : DbContext
    {
        public DbSet<People> Peoples { get; set; }
    }
}
