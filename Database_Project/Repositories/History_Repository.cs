﻿using CodeFirstDataBase;
using Project_Database.Contexts;
using Project_Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Project_Database.Repository
{
    public class History_Repository : Program_Repository<History, int>, IHistory
    {
        private readonly Project_DbContext _dbContext;

        public History_Repository(Project_DbContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<History>> FindAsync(int number)
        {
            return await _dbContext.Histories.Where(x => x.PK_ID.Equals(number)).ToListAsync();
        }

        public async Task<IEnumerable<History>> FindAllAsync()
        {
            return await _dbContext.Histories.ToListAsync<History>();
        }
    }
}
