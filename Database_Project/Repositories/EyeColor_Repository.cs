﻿using CodeFirstDataBase;
using Project_Database.Contexts;
using Project_Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Project_Database.Repository
{
    public class EyeColor_Repository : Program_Repository<EyeColor, int>, IEyeColor
    {
        private readonly Project_DbContext _dbContext;

        public EyeColor_Repository(Project_DbContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<EyeColor>> FindAsync(int number)
        {
            return await _dbContext.EyeColors.Where(x => x.PK_ID.Equals(number)).ToListAsync();
        }

        public async Task<IEnumerable<EyeColor>> FindAllAsync()
        {
            return await _dbContext.EyeColors.ToListAsync<EyeColor>();
        }
    }
}
