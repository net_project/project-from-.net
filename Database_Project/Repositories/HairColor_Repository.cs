﻿using CodeFirstDataBase;
using Project_Database.Contexts;
using Project_Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Project_Database.Repository
{
    public class HairColor_Repository : Program_Repository<HairColor, int>, IHairColor
    {
        private readonly Project_DbContext _dbContext;

        public HairColor_Repository(Project_DbContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<HairColor>> FindAsync(int number)
        {
            return await _dbContext.HairColors.Where(x => x.PK_ID.Equals(number)).ToListAsync();
        }

        public async Task<IEnumerable<HairColor>> FindAllAsync()
        {
            return await _dbContext.HairColors.ToListAsync<HairColor>();
        }
    }
}
