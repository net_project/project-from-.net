﻿using CodeFirstDataBase;
using Project_Database.Contexts;
using Project_Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Project_Database.Repository
{
    public class People_Repository : Program_Repository<People, int>, IPeople
    {
        private readonly Project_DbContext _dbContext;

        public People_Repository(Project_DbContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }
 
        public async Task<IEnumerable<People>> FindAsync(int number)
        {
            return await _dbContext.Peoples.Where(x => x.PK_ID.Equals(number)).ToListAsync();
        }

        public async Task<IEnumerable<People>> FindAllAsync()
        {
            return await _dbContext.Peoples.ToListAsync<People>();
        }

        public async Task<IEnumerable<People>> SelectAllPeople()
        {
            // Display all Peoples from the database 
            return await _dbContext.Peoples.Include("EyeColor").Include("HairColor").Include("User").ToListAsync();
        }

        public async Task<IEnumerable<People>> SelectAllPeople(System.Linq.Expressions.Expression<Func<People, bool>> predicate)
        {
            // Display all Peoples from the database 
            return await _dbContext.Peoples.Include("EyeColor").Include("HairColor").Include("User").Where(predicate).ToListAsync();
        }
    }
}
