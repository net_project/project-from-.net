﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Enums
{
    public enum OperationTypePerson_Enum
    {
        Register,
        LogIn,
        Add, //only for admin- adding users without logins
        Edit,
        Delete,
        Search
    }
}
