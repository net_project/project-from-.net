﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database.Enums
{
    public enum OperationTypeHistory_Enum
    {
        Get,
        Delete, //all "real", all "fake", from date to date
        DeleteAll
    }
}
