﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstDataBase
{
    public class Image
    {
        [Key]
        public int PK_ID { get; private set; }
        [Required]
        [Column(TypeName = "image")]
        public byte[]  image { get; set; }
        public virtual People People { get; set; }
    }
}
