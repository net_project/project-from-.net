﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstDataBase
{
    public class HairColor
    {
        public HairColor()
        {
            Peoples = new HashSet<People>();
        }
        [Key]
        public int PK_ID { get; private set; }
        public string Color { get; set; }
        public virtual ICollection<People> Peoples { get; set; }
    }
}
