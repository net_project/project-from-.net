﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstDataBase
{
    public class User
    {
        public User()
        {
            Histories = new HashSet<History>();
        }
        [Key]
        public int PK_ID { get; private set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool isAdmin { get; set; }
        public virtual ICollection<History> Histories { get; set; }
    }
}
