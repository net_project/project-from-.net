﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstDataBase
{
    public class People
    {
        public People()
        {
            Images = new HashSet<Image>();
        }
        [Key]
        public int PK_ID { get; private set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public bool isReal { get; set; }
        public virtual EyeColor EyeColor { get; set; }
        public virtual HairColor HairColor { get; set; }
        public virtual User User { get; set; }
        [Required]
        [Column(TypeName = "image")]
        public byte[] ProfileImage { get; set; }
        public virtual ICollection<Image> Images { get; set; }
    }
}
