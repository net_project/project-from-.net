﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstDataBase
{
    public class History
    {
        [Key]
        public int PK_ID { get; private set; }
        public DateTime Date { get; set; }
        public virtual People People { get; set; }
        public virtual User User { get; set; }
    }
}
