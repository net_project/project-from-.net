﻿using CodeFirstDataBase;
using Emgu.CV;
using Emgu.CV.Structure;
using Project_Database.Contexts;
using Project_Database.DataSets;
using Project_Database.Functions;
using Project_Database.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Database
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main start");
            //try
            //{
                Task t = MainAsync2(args);
                t.ContinueWith((str) =>
                {
                    Console.WriteLine(str.Status.ToString());
                    Console.WriteLine("Main end");
                });
                t.Wait();
            //}
            //catch (Exception dbEx)
            //{
            //    DbEntityValidationException exdb = (DbEntityValidationException)dbEx.InnerException;
            //    foreach (var validationErrors in exdb.EntityValidationErrors)
            //    {
            //        foreach (var validationError in validationErrors.ValidationErrors)
            //        {
            //            Trace.TraceInformation("Property: {0} Error: {1}",
            //                                    validationError.PropertyName,
            //                                    validationError.ErrorMessage);
            //        }
            //    }
            //}
            //Task t1 = MainAsync1(args);
            //t1.ContinueWith((str) =>
            //{
            //    Console.WriteLine(str.Status.ToString());
            //    Console.WriteLine("Main end");
            //});
            //t1.Wait();
        }

        static async Task MainAsync(string[] args)
        {
            var db = new Project_DbContext();
            // Create and save a new Person 
            Console.Write("Enter Your name: ");
            var name = Console.ReadLine();
            Console.Write("Enter Your surname: ");
            var surname = Console.ReadLine();

            byte[] mybyt = new byte[] { 1, 2 };
            string a = "";

            var person = new People { Name = name, Surname = surname, isReal = true };
            var eyecolor = new EyeColor { Color = ConsoleColor.Green.ToString() };
            var haircolor = new HairColor { Color = ConsoleColor.Black.ToString() };
            var history = new History { Date = DateTime.Now };
            var user = new User { Username = "wr", Password = "rw", isAdmin = true };
            var peo = new People { Name = "Name", Surname = "Surname", isReal = false };
            var image = new Image { image = mybyt };

            var PeopleRepository = new People_Repository(db);
            PeopleRepository.AddToContext(person);

            db.HairColors.Add(haircolor);
            db.EyeColors.Add(eyecolor);
            db.Histories.Add(history);
            db.Users.Add(user);
            db.Images.Add(image);
            image.People = person;
            person.User = user;
            person.EyeColor = eyecolor;
            peo.EyeColor = eyecolor;
            person.HairColor = haircolor;
            peo.HairColor = haircolor;
            db.Peoples.Add(person);
            person.Images.Add(image);
            db.Peoples.Add(peo);
            history.User = user;
            history.People = peo;
            user.Histories.Add(history);
            db.SaveChanges();

            // Display all Peoples from the database 
            //var Query = db.Peoples.Include("EyeColor").Include("HairColor").Include("User").ToList(); //All(p=>p.EyeColor.Color=="Blue");

            var Peoples = await PeopleRepository.FindAllAsync();
            var Peoples3 = await PeopleRepository.SelectAllPeople(p => p.Name == "Ada");
            var Peoples2 = Peoples.Where(p => p.Name == "Ada").ToList();
            Peoples2 = Peoples2.Where(p => p.Surname == "W").ToList();

            //var student = L2EQuery.FirstOrDefault<Student>();

            Console.WriteLine("All people in the database:");
            foreach (var item in Peoples2)
            {
                Console.WriteLine("ID:" + item.PK_ID);
                Console.WriteLine("Name:" + item.Name);
                Console.WriteLine("Surname:" + item.Surname);
                if (item.EyeColor != null)
                    Console.WriteLine("EyeColor:" + item.EyeColor.Color);
                if (item.HairColor != null)
                    Console.WriteLine("HairColor:" + item.HairColor.Color);
                if (item.Images != null && item.Images.Count != 0)
                {
                    Console.WriteLine("Image:" + item.Images.ElementAt(0).image.ToString());
                    Console.WriteLine("Image:" + item.Images.ElementAt(0).People.Name);
                }
                if (item.isReal != null)
                    Console.WriteLine("Real:" + item.isReal);
                if (item.User != null)
                    Console.WriteLine("User:" + item.User.Username);
                if (item.User != null)
                    Console.WriteLine("User:" + item.User.Password);
                if (item.User != null)
                {
                    Console.WriteLine("User:" + item.User.isAdmin);
                    if (item.User.Histories != null && item.User.Histories.Count != 0)
                    {
                        if (item.User.Histories.ElementAt(0).People != null)
                        {
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.Name);
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.Surname);
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.isReal);
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.EyeColor.Color);
                        }
                    }
                }
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
        static async Task MainAsync1(string[] args)
        {
            var db1 = new Project_DbContext();
            // Create and save a new Person 
            Console.Write("Enter Your name: ");
            var name1 = Console.ReadLine();
            Console.Write("Enter Your surname: ");
            var surname1 = Console.ReadLine();

            byte[] mybyt1 = new byte[] { 1, 2 };
            string a1 = "";

            var person1 = new People { Name = name1, Surname = surname1, isReal = true };
            var eyecolor1 = new EyeColor { Color = ConsoleColor.Green.ToString() };
            var haircolor1 = new HairColor { Color = ConsoleColor.Black.ToString() };
            var history1 = new History { Date = DateTime.Now };
            var user1 = new User { Username = "wr", Password = "rw", isAdmin = true };
            var peo1 = new People { Name = "Namer", Surname = "Surnamer", isReal = false };
            var image1 = new Image { image = mybyt1 };

            var PeopleRepository1 = new People_Repository(db1);
            PeopleRepository1.AddToContext(person1);

            db1.HairColors.Add(haircolor1);
            db1.EyeColors.Add(eyecolor1);
            db1.Histories.Add(history1);
            db1.Users.Add(user1);
            db1.Images.Add(image1);
            image1.People = person1;
            person1.User = user1;
            person1.EyeColor = eyecolor1;
            peo1.EyeColor = eyecolor1;
            person1.HairColor = haircolor1;
            peo1.HairColor = haircolor1;
            db1.Peoples.Add(person1);
            person1.Images.Add(image1);
            db1.Peoples.Add(peo1);
            history1.User = user1;
            history1.People = peo1;
            user1.Histories.Add(history1);
            db1.SaveChanges();

            // Display all Peoples from the database 
            //var Query = db.Peoples.Include("EyeColor").Include("HairColor").Include("User").ToList(); //All(p=>p.EyeColor.Color=="Blue");

            var Peoples1 = await PeopleRepository1.FindAllAsync();
            var Peoples31 = await PeopleRepository1.SelectAllPeople(p => p.Name == "Ada");
            var Peoples21 = Peoples1.Where(p => p.Name == "Ada").ToList();
            Peoples21 = Peoples21.Where(p => p.Surname == "W").ToList();

            //var student = L2EQuery.FirstOrDefault<Student>();

            Console.WriteLine("All people in the database1:");
            foreach (var item in Peoples21)
            {
                Console.WriteLine("Name:" + item.Name);
                Console.WriteLine("Surname:" + item.Surname);
                if (item.EyeColor != null)
                    Console.WriteLine("EyeColor:" + item.EyeColor.Color);
                if (item.HairColor != null)
                    Console.WriteLine("HairColor:" + item.HairColor.Color);
                if (item.Images != null && item.Images.Count != 0)
                {
                    Console.WriteLine("Image:" + item.Images.ElementAt(0).image.ToString());
                    Console.WriteLine("Image:" + item.Images.ElementAt(0).People.Name);
                }
                if (item.isReal != null)
                    Console.WriteLine("Real:" + item.isReal);
                if (item.User != null)
                    Console.WriteLine("User:" + item.User.Username);
                if (item.User != null)
                    Console.WriteLine("User:" + item.User.Password);
                if (item.User != null)
                {
                    Console.WriteLine("User:" + item.User.isAdmin);
                    if (item.User.Histories != null && item.User.Histories.Count != 0)
                    {
                        if (item.User.Histories.ElementAt(0).People != null)
                        {
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.Name);
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.Surname);
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.isReal);
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.EyeColor.Color);
                        }
                    }
                }
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
        static async Task MainAsync2(string[] args)
        {
            //System.Drawing.Image compare = System.Drawing.Image.FromFile(@"C:\Users\Ada\Documents\Visual Studio 2012\Projects\Database_Project\Database_Project\bin\Debug\fejsys\twarz2.jpg");
            //System.Drawing.Image random = System.Drawing.Image.FromFile(@"C:\Users\Ada\Documents\Visual Studio 2012\Projects\Database_Project\Database_Project\bin\Debug\fejsys\twarz3.jpg");
            //System.Drawing.Image tofind = System.Drawing.Image.FromFile(@"C:\Users\Ada\Documents\Visual Studio 2012\Projects\Database_Project\Database_Project\bin\Debug\fejsys\twarz5.jpg");

            Image<Bgr, Byte> compare = new Image<Bgr, Byte>("C:/Users/Ada/Documents/Visual Studio 2012/Projects/Database_Project/Database_Project/bin/Debug/fejsys/twarz2.jpg");
            Image<Bgr, Byte> random = new Image<Bgr, Byte>("C:/Users/Ada/Documents/Visual Studio 2012/Projects/Database_Project/Database_Project/bin/Debug/fejsys/twarz3.jpg");
            Image<Bgr, Byte> tofind = new Image<Bgr, Byte>("C:/Users/Ada/Documents/Visual Studio 2012/Projects/Database_Project/Database_Project/bin/Debug/fejsys/twarz5.jpg");

            Converters.ByteImage_Converters bc = new Converters.ByteImage_Converters();

            byte[] compareb = bc.ImageToByte(compare.ToBitmap(), System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] randomb = bc.ImageToByte(random.ToBitmap(), System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] tofindb = bc.ImageToByte(tofind.ToBitmap(), System.Drawing.Imaging.ImageFormat.Jpeg);
            
            var db = new Project_DbContext();
            var HistoryRepository1 = new History_Repository(db);
            var PeopleRepository1 = new People_Repository(db);
            var ImageRepository1 = new Image_Repository(db);
            var UserRepository1 = new User_Repository(db);
            //var users = await UserRepository.FindAllAsync();
            //foreach (var item in users)
            //{
            //    UserRepository.Delete(item);
            //}
            //db.Images.Add(new Image { image = new byte[1] });
            db.SaveChanges();
            Person_DataSet person = new Person_DataSet();
            person.eyecolor = "grey";
            person.haircolor = "violet";
            person.image = randomb;
            person.isReal = true;
            person.name = "Test";
            person.surname = "T";
            person.username = "user6";
            person.password = "pwd";
            person.uid = 2;
            person.id = 2;
            Person_DataSet person3 = new Person_DataSet();
            person3.eyecolor = "grey";
            person3.haircolor = "violet";
            person3.image = tofindb;
            person3.name = "Test";
            person3.surname = "T";
            person3.username = null;
            person3.password = null;
            Person_DataSet person1 = new Person_DataSet();
            person1.eyecolor = "grey";
            person1.haircolor = "violet";
            person1.image = randomb;
            person1.isReal = true;
            person1.name = "Test";
            person1.surname = "T";
            person1.username = "user";
            person1.password = "pwd";
            //person1.id = 2;
            //person1.uid = 2;
            Person_DataSet person4 = new Person_DataSet();
            person4.eyecolor = "grey";
            person4.haircolor = "violet";
            person4.image = randomb;
            person4.isReal = true;
            person4.name = "Test";
            person4.surname = "T";
            person4.username = "user2";
            person4.password = "pwd";
            Person_DataSet person2 = new Person_DataSet();
            person2.eyecolor = "grey";
            person2.haircolor = "violet";
            person2.image = randomb;
            person2.isReal = true;
            person2.name = "Test";
            person2.surname = "T";
            person2.username = "user3";
            person2.password = "pwd";
            person2.uid = 3;
            person2.id = 3;
            var response = await ComplexPerson_Functions.Edit(person);
            //var response1 = await ComplexPerson_Functions.Register(person1);
            //var response4 = await ComplexPerson_Functions.Register(person4);
            //var response2 = await ComplexPerson_Functions.Edit(person2);
            //var response3 = ComplexHistory_Functions.(response);
            var response5 = await ComplexPerson_Functions.Search(response, person3);
            var Peoples1 = await PeopleRepository1.FindAllAsync();
            Peoples1 = Peoples1.Where(x => x.isReal == true);
            var History = await HistoryRepository1.FindAllAsync();
            foreach (var item in History)
            {
                Console.WriteLine(" ");
                Console.WriteLine("Searched:" + item.People.PK_ID);
                Console.WriteLine("When:" + item.Date);
                Console.WriteLine("Searcher:" + item.User.PK_ID);
            }
            var Users = await UserRepository1.FindAllAsync();
            foreach (var item in Users)
            {
                Console.WriteLine(" ");
                Console.WriteLine("UserID:" + item.PK_ID);
                Console.WriteLine("UN:" + item.Username);
                Console.WriteLine("PS:" + item.Password);
            }
            //Console.WriteLine("PersonID:" + response.id);
            foreach (var item in Peoples1)
            {

                Console.WriteLine(" ");
                Console.WriteLine("Name:" + item.Name);
                Console.WriteLine("PersonID:" + item.PK_ID);
                Console.WriteLine("Surname:" + item.Surname);
                if (item.EyeColor != null)
                {
                    Console.WriteLine("EyeColor:" + item.EyeColor.Color);
                    Console.WriteLine("EyeColorID:" + item.EyeColor.PK_ID);
                }
                if (item.HairColor != null)
                {
                    Console.WriteLine("HairColor:" + item.HairColor.Color);
                    Console.WriteLine("HairColorID:" + item.HairColor.PK_ID);
                }
                if (item.ProfileImage != null)
                {
                    Console.WriteLine("HairColor:" + item.ProfileImage.Count());
                    Console.WriteLine("HairColorID:" + item.ProfileImage.ToString());
                }
                if (item.Images != null && item.Images.Count != 0)
                {
                    Console.WriteLine("Image:" + item.Images.ElementAt(0).image.ToString());
                    Console.WriteLine("Image:" + item.Images.ElementAt(0).People.Name);
                    Console.WriteLine("ImageID:" + item.Images.ElementAt(0).PK_ID);
                    var Images1 = await ImageRepository1.FindAsync(item.Images.ElementAt(0).PK_ID);
                    Console.WriteLine("ImagePID:" + Images1.ElementAt(0).People.PK_ID);
                }
                if (item.isReal != null)
                    Console.WriteLine("Real:" + item.isReal);
                if (item.User != null)
                    Console.WriteLine("User:" + item.User.Username);
                if (item.User != null)
                    Console.WriteLine("User:" + item.User.Password);
                if (item.User != null)
                {
                    Console.WriteLine("User:" + item.User.isAdmin);
                    if (item.User.Histories != null && item.User.Histories.Count != 0)
                    {
                        if (item.User.Histories.ElementAt(0).People != null)
                        {
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.Name);
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.Surname);
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.isReal);
                            Console.WriteLine("History:" + item.User.Histories.ElementAt(0).People.EyeColor.Color);
                        }
                    }
                }
            }
            //if (response3 != null)
            //{
            //    Console.WriteLine(" ");
            //    Console.WriteLine("Name:" + response3.name);
            //    Console.WriteLine("PersonID:" + response3.id);
            //    Console.WriteLine("Surname:" + response3.surname);
            //    if (response3.eyecolor != null)
            //    {
            //        Console.WriteLine("EyeColor:" + response3.eyecolor);
            //    }
            //    if (response3.haircolor != null)
            //    {
            //        Console.WriteLine("HairColor:" + response3.haircolor);
            //    }
            //    if (response3.image != null)
            //    {
            //        Console.WriteLine("Image:" + response3.image);
            //    }
            //    if (response3.isReal != null)
            //        Console.WriteLine("Real:" + response3.isReal);
            //    if (response3.username != null)
            //        Console.WriteLine("User:" + response3.username);
            //    if (response3.password != null)
            //        Console.WriteLine("User:" + response3.password);
            //    if (response3.uid != null)
            //    {
            //        Console.WriteLine("User:" + response3.uid);
            //    }
            //}

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
