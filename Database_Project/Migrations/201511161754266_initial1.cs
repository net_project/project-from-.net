namespace Project_Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.People", "User_PK_ID", "dbo.Users");
            AddForeignKey("dbo.People", "User_PK_ID", "dbo.Users", "PK_ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.People", "User_PK_ID", "dbo.Users");
            AddForeignKey("dbo.People", "User_PK_ID", "dbo.Users", "PK_ID");
        }
    }
}
