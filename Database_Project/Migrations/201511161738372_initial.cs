namespace Project_Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.People", "ProfileImage", c => c.Binary(nullable: false));
            AlterColumn("dbo.Images", "image", c => c.Binary(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Images", "image", c => c.Binary(maxLength: 4000));
            AlterColumn("dbo.People", "ProfileImage", c => c.Binary(maxLength: 4000));
        }
    }
}
