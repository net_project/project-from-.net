namespace Project_Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EyeColors",
                c => new
                    {
                        PK_ID = c.Int(nullable: false, identity: true),
                        Color = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.PK_ID);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        PK_ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 4000),
                        Surname = c.String(maxLength: 4000),
                        isReal = c.Boolean(nullable: false),
                        ProfileImage = c.Binary(maxLength: 4000),
                        EyeColor_PK_ID = c.Int(),
                        HairColor_PK_ID = c.Int(),
                        User_PK_ID = c.Int(),
                    })
                .PrimaryKey(t => t.PK_ID)
                .ForeignKey("dbo.EyeColors", t => t.EyeColor_PK_ID)
                .ForeignKey("dbo.HairColors", t => t.HairColor_PK_ID)
                .ForeignKey("dbo.Users", t => t.User_PK_ID)
                .Index(t => t.EyeColor_PK_ID)
                .Index(t => t.HairColor_PK_ID)
                .Index(t => t.User_PK_ID);
            
            CreateTable(
                "dbo.HairColors",
                c => new
                    {
                        PK_ID = c.Int(nullable: false, identity: true),
                        Color = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.PK_ID);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        PK_ID = c.Int(nullable: false, identity: true),
                        image = c.Binary(maxLength: 4000),
                        People_PK_ID = c.Int(),
                    })
                .PrimaryKey(t => t.PK_ID)
                .ForeignKey("dbo.People", t => t.People_PK_ID)
                .Index(t => t.People_PK_ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        PK_ID = c.Int(nullable: false, identity: true),
                        Username = c.String(maxLength: 4000),
                        Password = c.String(maxLength: 4000),
                        isAdmin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PK_ID);
            
            CreateTable(
                "dbo.Histories",
                c => new
                    {
                        PK_ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        People_PK_ID = c.Int(),
                        User_PK_ID = c.Int(),
                    })
                .PrimaryKey(t => t.PK_ID)
                .ForeignKey("dbo.People", t => t.People_PK_ID)
                .ForeignKey("dbo.Users", t => t.User_PK_ID)
                .Index(t => t.People_PK_ID)
                .Index(t => t.User_PK_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.People", "User_PK_ID", "dbo.Users");
            DropForeignKey("dbo.Histories", "User_PK_ID", "dbo.Users");
            DropForeignKey("dbo.Histories", "People_PK_ID", "dbo.People");
            DropForeignKey("dbo.Images", "People_PK_ID", "dbo.People");
            DropForeignKey("dbo.People", "HairColor_PK_ID", "dbo.HairColors");
            DropForeignKey("dbo.People", "EyeColor_PK_ID", "dbo.EyeColors");
            DropIndex("dbo.Histories", new[] { "User_PK_ID" });
            DropIndex("dbo.Histories", new[] { "People_PK_ID" });
            DropIndex("dbo.Images", new[] { "People_PK_ID" });
            DropIndex("dbo.People", new[] { "User_PK_ID" });
            DropIndex("dbo.People", new[] { "HairColor_PK_ID" });
            DropIndex("dbo.People", new[] { "EyeColor_PK_ID" });
            DropTable("dbo.Histories");
            DropTable("dbo.Users");
            DropTable("dbo.Images");
            DropTable("dbo.HairColors");
            DropTable("dbo.People");
            DropTable("dbo.EyeColors");
        }
    }
}
