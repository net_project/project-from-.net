﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.ComponentModel;

namespace Project_Database.Converters
{
    public class ByteImage_Converters
    {
        public byte[] ImageToByte(Image imageIn, ImageFormat format)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, format);
            return ms.ToArray();
        }

        public Image ByteToImage(byte[] byteIn)
        {
            MemoryStream ms = new MemoryStream(byteIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public byte[] GetBytes(string stringIn)
        {
            byte[] b1 = System.Text.Encoding.UTF8.GetBytes(stringIn);
            return b1;
        }

        public string GetString(byte[] byteIn)
        {
            string result = System.Text.Encoding.UTF8.GetString(byteIn);
            return result;
        }

        public Image<Gray, Byte> ByteToGray(byte[] img)
        {
            Bitmap masterImage;
            using (var ms = new MemoryStream(img))
            {
                masterImage = new Bitmap(ms);
            }

            // Normalizing it to grayscale
            Image<Gray, Byte> imggray = new Image<Gray, Byte>(masterImage);
            return imggray;
        }

        public Image<Bgr, Byte> ImageToBgr(Image img)
        {
            return new Image<Bgr, Byte>(new Bitmap(img));
        }

        public Image<Bgr, Byte> ByteToBgr(byte[] img)
        {
            //for (int imageIdx = 0; imageIdx < img.Count() && imageIdx + 2 < img.Count(); imageIdx += 3)
            //{
            //    byte tempRGB = img[imageIdx];
            //    img[imageIdx] = img[imageIdx + 2];
            //    img[imageIdx + 2] = tempRGB;
            //}
            //Bitmap masterImage;
            //using (var ms = new MemoryStream(img))
            //{
            //    masterImage = new Bitmap(ms);
            //}
            TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
            Bitmap bitmap1 = (Bitmap)tc.ConvertFrom(img);

            // Normalizing it to grayscale
            Image<Bgr, Byte> imgbgr = new Image<Bgr, Byte>(bitmap1);
            return imgbgr;
        }
    }
}
