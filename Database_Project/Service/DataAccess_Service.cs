﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project_Database.DataSets;
using Project_Database.Functions;
using System.Data.Entity;
using Project_Database.Contexts;

namespace Project_Database.Service
{
    public class DataAccess_Service
    {
        public static Project_DbContext GetDbContext()
        {
            return new Project_DbContext();
        }
        //function to distinguish data-sets, returns true on human :P
        public bool CheckDSType(DataSet data)
        {
            switch(data.dstype)
            {
                case Enums.DataSetType_Enum.PersonDS:
                {
                    return true;
                }
                case Enums.DataSetType_Enum.HistoryDS:
                {
                    return false;
                }
                default:
                {
                    return false;
                }
            }
        }

        public int CheckHistoryOpType(History_DataSet data)
        {
            switch (data.hop)
            {
                case Enums.OperationTypeHistory_Enum.Get:
                    {
                        return 0;
                    }
                case Enums.OperationTypeHistory_Enum.Delete:
                    {
                        return 1;
                    }
                case Enums.OperationTypeHistory_Enum.DeleteAll:
                    {
                        return 2;
                    }
                default:
                    {
                        return 0;
                    }
            }
        }

        public int CheckPersonOpType(Person_DataSet data)
        {
            switch (data.pop)
            {
                case Enums.OperationTypePerson_Enum.Add:
                    {
                        return 0;
                    }
                case Enums.OperationTypePerson_Enum.Register:
                    {
                        return 1;
                    }
                case Enums.OperationTypePerson_Enum.Search:
                    {
                        return 2;
                    }
                case Enums.OperationTypePerson_Enum.LogIn:
                    {
                        return 3;
                    }
                case Enums.OperationTypePerson_Enum.Edit:
                    {
                        return 4;
                    }
                case Enums.OperationTypePerson_Enum.Delete:
                    {
                        return 5;
                    }
                default:
                    {
                        return 2;
                    }
            }
        }

        public static async Task<Person_DataSet> PrepareResponseToRequestPerson(DataSet data, DataSet admin_searcher_data)
        {
            var per_data = data as Person_DataSet;
            switch (per_data.pop)
            {
                //admin only!
                case Enums.OperationTypePerson_Enum.Add:
                    {
                        //function receiving per_data, filling db and returning the data
                        return await ComplexPerson_Functions.AdminAdd((Person_DataSet)admin_searcher_data,per_data);
                    }
                case Enums.OperationTypePerson_Enum.Register:
                    {
                        //function receiving per_data, filling db and returning the data
                        return await ComplexPerson_Functions.Register(per_data);
                    }
                case Enums.OperationTypePerson_Enum.Search:
                    {
                        //function receiving per_data, filling it and returning
                        return await ComplexPerson_Functions.Search((Person_DataSet)admin_searcher_data,per_data);
                    }
                case Enums.OperationTypePerson_Enum.LogIn:
                    {
                        //function receiving per_data, filling it and returning
                        return await ComplexPerson_Functions.LogIn(per_data);
                    }
                case Enums.OperationTypePerson_Enum.Edit:
                    {
                        //function receiving per_data, reading it, changing db and returning
                        return await ComplexPerson_Functions.Edit(per_data);
                    }
                case Enums.OperationTypePerson_Enum.Delete:
                    {
                        //function receiving per_data, deleting it and returning empty class
                        return await ComplexPerson_Functions.Delete(per_data);
                    }
                default:
                    {
                        return await ComplexPerson_Functions.Search((Person_DataSet)admin_searcher_data, per_data);
                    }
            }
        }
        public static async Task<List<History_DataSet>> PrepareResponseToRequestHistory(DataSet data)
        {
            var his_data = data as Person_DataSet;
            switch (his_data.hop)
            {
                case Enums.OperationTypeHistory_Enum.Get:
                    {
                        //function receiving his_data, filling it and returning
                        return await ComplexHistory_Functions.Get(his_data);
                    }
                case Enums.OperationTypeHistory_Enum.Delete:
                    {
                        //function receiving his_data and removing it based on the removeFalse boolean
                        return await ComplexHistory_Functions.Delete(his_data);
                    }
                case Enums.OperationTypeHistory_Enum.DeleteAll:
                    {
                        //function receiving his_data and removing it all
                        return await ComplexHistory_Functions.DeleteAll(his_data);
                    }
                default:
                    {
                        return await ComplexHistory_Functions.Get(his_data);
                    }
            }
        }
    }
}
